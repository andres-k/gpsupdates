﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace GPSUpdates
{
    class Program
    {
        const string url = "http://soiduplaan.tallinn.ee/gps.txt", filename = "gps.txt";

        static void Main(string[] args)
        {
            string firstHash = GetChecksum();
            Console.WriteLine(firstHash);
            if (String.IsNullOrEmpty(firstHash))
            {
                Console.WriteLine("Error retrieving file from server");
                return;
            }
            Console.WriteLine("Waiting 60 seconds before downloading new gps info and calculating MD5 hash");
            Thread.Sleep(1000 * 60);
            string secondHash = GetChecksum();
            
            Console.WriteLine(secondHash);
            
            if (String.IsNullOrEmpty(secondHash))
            {
                Console.WriteLine("Error retrieving file from server");
                Console.WriteLine("Press the Enter key to exit the program... ");
                Console.ReadLine();
                return;
            }

            FinalVerdict(firstHash, secondHash);
            Console.WriteLine("Press the Enter key to exit the program... ");
            Console.ReadLine();
            Console.WriteLine("Terminating the application...");
        }

        private static void FinalVerdict(string firstHash, string secondHash)
        {
            if (firstHash == secondHash)
            {
                Console.WriteLine("Expected hashes to be different but they weren't");
            }
            else
            {
                Console.WriteLine("Expected hashes to be different and they were");
            }
        }

        private static string GetChecksum()
        {
            bool failure = DownloadGPSInfo();
            if(failure)
            {
                return "";
            }
            return CalculateMD5Hash();
        }

        private static bool DownloadGPSInfo()
        {
            using (var client = new WebClient())
            {
                try
                {
                    client.DownloadFile(url, filename);
                    return false;
                }
                catch(System.Net.WebException)
                {
                    Console.WriteLine("Problem retrieving file from " + url);
                    return true;
                }

            }
        }

        private static string CalculateMD5Hash()
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream));
                }
            }
        }

    }
}
